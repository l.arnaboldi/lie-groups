\section{Manifolds}
\begin{definition}[Homeomorphism] \index{homeomorphism}
	Let $A$ and $B$ two Hausdorff topological space. A function $\phi\colon A \to B$
	is an \emph{homeomorphism} if it is a bijection and both $\phi$ and $\phi^{-1}$
	are continuous respect their topologies. $A$ and $B$ are said to be
	\emph{homeomorphic}.
\end{definition}
\begin{definition}[manifold] \index{manifold} \label{def:manifold}
	A \emph{manifold} of dimension $\dim{M} = N$ is a Hausdorff topological space
	which is locally holomorphic to $\R^N$; namely, given 
	$p \in M$ there exists an open neighborhood $U_p$ of $p$ and an open subset
	$V$ of $\R^N$ such that $U_p$ and $V$ are homeomorphic.
\end{definition} 
This definition of manifold doesn't give us the concept of \emph{coordinates} on
$M$, not even locally. Since coordinates are necessary to define the differentiability
of functions, we introduce the concept of \emph{chart} that give us to parametrize
neighbourhoods.
\begin{definition}[Chart] \index{chart}
	Let $M$ a manifold of dimansion $N$. A \emph{chart} is a couple $(U, \varphi)$ where $U$ is a open subset of $M$ and $\varphi$ is an homeomorphis from $U$ to an open subset of $\R^N$.
\end{definition}
\begin{definition}[Atlas] \index{atlas}
	An \emph{atlas} of a manifold $M$ is a set of charts
	$\left\{(U_i, \varphi_i)\right\}$ such that $\bigcup_{i}U_i = M$.
\end{definition}

The definition of manifold given by \ref{def:manifold} allows really irregular 
structures. We usally prefer to handle more regular ones, so we define the 
\emph{smooth manifold}.
\begin{definition}[Smooth manifold] \index{manifold!smooth} \label{def:manifold}
	A manifold $M$ is said to be a \emph{smooth manifold} if it has an atlas
	such that given two charts $(U_i, \varphi_i)$ and $(U_j, \varphi_j)$ with 
	$U_i \cap U_j \not= \emptyset$, the \emph{transition functions}
	\[g_{ij} \equiv \varphi_j \circ \varphi^{-1}_j \]
	are $C^\infty$.
\end{definition}

Now we have the concept of smoothness we can also define what differentiable mean
on a manifold.
\begin{definition}[Smooth function to $\R$]
	A function $f \colon M \to \R$ is said to be $C^k$ in $p \in M$ when, given a
	chart $(U, \varphi)$ containing $p$, the function $f \circ \varphi^{-1}$ is
	$C^k$\footnote{Strictily speaking we don't need a smooth manifold for have 
		$C^k$ functions, but it is sufficent that transition functions are $C^k$
		too.}.\\
	If $f \circ \varphi^{-1}$ is $C^\infty$ the function $f$ is also said to be 
	\emph{smooth}; the set of smooth functions on $M$ is denoted by $C^\infty(M)$.
\end{definition}
Note that the map $f \circ \varphi^{-1}$ goes from an open subset of $\R^N$ to 
$\R$ so is well-defined what $C^k$ means.
\begin{definition}[Smooth function between manifolds]
	Let $M_1$ and $M_2$ two smooth manifolds. A map $f$ from $M_1$ to $M_2$ is said
	to be \emph{smooth} at $p \in M_1$ if, given $(U,\varphi)$ and $(V, \psi)$
	charts of $p$ and $f(p) \in M_2$ respectivily, the function $\psi \circ f \circ \varphi^{-1}$ is $C^\infty$.
\end{definition}
\begin{definition}[Diffeomorphism]
	Let $M_1$ and $M_2$ two smooth manifolds. A map $f$ from $M_1$ to $M_2$ is
	said to be a \emph{diffeomorphism} if it is bijective and both $f$ and $f^{-1}$
	are smooth.
\end{definition}


\subsection{Tanget Space and bundle}
One way to define the tanget space in manifolds is using the velocity of the curves.
\begin{definition}[Differentiable curve] \index{differentiable curve}
	A function $\gamma \colon I \to M$, where $I$ is an open interval of $\R$, 
	is a \emph{differentiable curve} in $x \in I$ if given a chart $(U, \varphi)$ 
	such that $\gamma(x) \in U$, the function $\varphi \circ \gamma$ is $C^1$.
\end{definition}
We can think the function $\gamma(t)$ as the ``position'' at time $t$. Note that the
condition does not depend on the chart choosen.\\
Let now $p \in M$ and $\Gamma_p$ the set of all differentiable passing throught $p$ 
at time
$t = 0$:
\[\Gamma_p = \left\{ \gamma \colon I \to M | \gamma(0) = p \right\} \]
\begin{definition}[Tangent space, version 1] \index{tangent space} \label{def:tangspace1}
	Let choose a chart $(U, \varphi)$ containing $p$. We say that two curves
	$\gamma_1, \gamma_2 \in \Gamma_p$ are equivalent if they have the same velocity
	vector in $p$:
	\[ \gamma_1 \sim \gamma_2 \iff 
	\left. \frac{\partial \left( \varphi \circ \gamma_1 \right) }{\partial t} \right|_{t=0} =
	\left. \frac{\partial \left( \varphi \circ \gamma_2 \right) }{\partial t} \right|_{t=0}.
	\]
	The set of equivalent class is called \emph{tangent space} and it is denoted with
	$T_pM$ \[T_pM \equiv \Gamma_p / \sim.\]
\end{definition}
It can be proven\footnote{See \cite{illinois} for details.} that this definition
is indipendent from the chart choosen and $T_pM$ is a vector space of dimension
$\dim{T_pM} = \dim{M}$.

This definition of tangent space is more intuitive than the others, but for our
purpose we need another one. The proof that these two definition are equivalent
won't be presented here. Let first of all define what is a \emph{derivation} on
manifolds.
\begin{definition}[Derivation at $p$] \index{derivation} \label{def:derivationatp}
	Let $p \in M$. A function $D \colon C^\infty(M) \to \R$ is said to be a 
	\emph{derivation} at $p$ if it has these two properties
	\begin{enumerate}
		\item \emph{linearty}, namely  \[D(\alpha f + \beta g) = \alpha D(f) + \beta D(g)
			\quad \forall \alpha, \beta \in \R, \text{ and } \forall f,g \in C^\infty(M); \]
		\item \emph{Leibniz rule} for the products 
		\[D(fg) = D(f)g(p) + f(p)D(g) \quad f,g \in C^\infty(M).\]
	\end{enumerate}
\end{definition}
In simple terms a derivation behaves on the manifold $M$ like a directional derivative
\footnote{This correspondece can be showed formally. For details see \cite{illinois}.}
does on $\R^n$. We can think a derivation as a \emph{tangent vector} at $p$, so we can
define the tangent space of $p$ as the set of all its derivators.
\begin{definition}[Tangent space, version 2] \index{tangent space}
	The \emph{tangent space} at $p$ $T_pM$ is the set of all the derivators at $p$.
\end{definition}
It's easy to see that $T_pM$ is a vector space with operations
\[(D_1 + D_2)(f) \overset{\mathrm{def}}{=} D_1(f) + D_2(f),\]
\[(\lambda D)(f) \overset{\mathrm{def}}{=} \lambda D(f).\]
We won't prove neither in this case that $\dim{T_pM} = \dim{M}$.
\begin{definition}[Tangent bundle] \index{tangent bundle}
	The \emph{tangent bundle} of $M$ is the disjoint union of tangent spaces
	\[TM = \bigcup_{p \in M}\{p\} \times T_pM.\]
\end{definition}
It's useful to define the projection $\pi$ from the tangent bundle to the manifold in
the natural way
\begin{align*}
	\pi\colon TM &\to M, \\
	(p,v) &\to p.
\end{align*}
The tangent bundle is also a smooth manifold and its dimension is $2\cdot \dim{M}$.

\subsection{Differentials}
The differential is generalization applied to manifolds of the \emph{Jacobian} for
functions from $\R^n$ to $\R^m$. In some sense it is  the best linear approximation
of a smooth function between manifolds near a certain point. 
\begin{definition}[Differential at $p$] \index{differential}
	Let $M_1$ and $M_2$ two smooth manifolds and $\phi\colon M_1 \to M_2$ a smooth
	function of them. The \emph{differential of $f$ at $p \in M_1$} is
	defined as a function $d\phi\colon T_pM_1 \to T_{\phi(p)}M_2$ such that
	\[\left(d\phi(v)\right)(g) = v(g \circ \phi) \quad 
	  \forall v \in T_pM_1, \quad
	  \forall g \in C^\infty(M_2).
	\]
\end{definition}
Note that we have to verify that $v(g \circ \phi)$ is a derivation at $\phi(p)$ to
say that \emph{differetial} is well-defined. We also mentioned before the definition
that the differential is a \emph{linear application}, so this property must also be
verified too, but we won't prove any of that here.
%TODO: fare davvero queste verifiche anche se sono una rottura...


\begin{theorem}[Chain Rule] \index{Chain Rule}
	Let $M_1$, $M_2$ and $M_3$ smooth manifolds; let $F\colon M_1 \to M_2$ and
	$G\colon M_2 \to M_3$ smooth functions of manifolds, then
	\[d(G \circ F )_p = dG_{F(p)} \circ dF_{p},\]
	for any $p \in M_1$.
	\begin{proof}
		Fix $p \in M_1$, $v \in T_pM_1$ and $h \in C^\infty(M_3)$. Then
		\begin{align*}
			\left(d(G\circ F)_p(v)\right)(f) 
			  &= v(f \circ (G\circ F)) = v((f \circ G) \circ F) = \\
			  &= \underbrace{\left(dF_p(v)\right)}_{w \in T_{F(p)}M_2}(f \circ G)
			                                                  = w(f \circ G) = \\
			  &= \left(dG_{F(p)}(w)\right)(f) =
			                   \left(dG_{F(p)}\left(dF_p(v)\right)\right)(f) = \\
			  &= \left(\left(d(G \circ F )_p\right)(v)\right)(f).
		\end{align*}
	\end{proof}
\end{theorem}
%TODO: il differenziale come apllicazione tra i tangent bundle

\subsection{Vector Fields}
We are now ready to define \emph{vector fields}: the idea is to assign to each point
of the manifold a vector from its tanget space. More formally:
\begin{definition}[Vector field] \index{vector field} \label{def:vectorfieldbundle}
	A \emph{vector field} is a map $X\colon M \to TM$ such that $\pi \circ X$ is
	the identity function.
\end{definition} 
We will point with $X_p$ the value of the vector field at $p$; we will treat
$X_p$ as an element of $T_pM$ (instead ofthe couple $\{p,v\}$), so it will be
a derivation.
Generally, we would like vector fields to have some smoothness properties when
$p$ varies in $M$. Therefore we say that a vector field is \emph{smooth} if $X$
is also a diffeomorphism. It's often useful to use the following alternative
definition for vector spaces.
\begin{definition}[Vector field] \index{vector field} \label{def:vectorfieldderiv}
	A \emph{vector field} is a \emph{linear} map $X\colon C^\infty(M)  \to C^\infty(M)$
	that respects the \emph{Leibniz rule}:
	\[X(fg) = X(f) \cdot g + f \cdot X(g) \quad f,g \in C^\infty(M).\]
\end{definition}
This kind of functions are also called \emph{derivations}, as the the definition
could suggest since they are really similator to functions defined in \ref{def:derivationatp}.
The colloction of all smooth vector fields on a smooth manifold $M$ is denoted by
$\X(M)$.

We will now prove the equivalence of this two definitions.
\begin{lemma} \label{lem:vectorfieldequiv}
	The definitions \ref{def:vectorfieldderiv} and \ref{def:vectorfieldbundle} for
	smooth vector vector field are equivalent.
	\begin{proof}
		Let $X\colon M \to TM$ a smooth vector field in the sense of \ref{def:vectorfieldbundle}.
		Let $\tilde{X}$ a map from $C^\infty(M)$ to the space of functions on $M$
		such that
		\[\left(\tilde{X}(f)\right)(p) = \left(X(p)\right)(f) \quad 
		  \forall p \in M, \text{ and } \forall f \in C^\infty(M).\]
		%TODO: end the proof.
	\end{proof}
\end{lemma}
Given a vector space $X$ defined as the \ref{def:vectorfieldbundle} we will denote
his definition as \ref{def:vectorfieldderiv} with $D_X$.
\subsubsection{Integral curves}
First of all we need to specify what is the derivative of a differential curve
at a certain point of a manifold. Let $\gamma$ a differential curve such that
$\gamma(0)= p\in M$. The derivative $\gamma'(0)$ is the representative element
of $\gamma$ in the tangent space $T_pM$, in the sense of definiton \ref{def:tangspace1}.
We are now ready to define integral curves of vector fields. 
\begin{definition}[Integral curve]
	An \emph{integral curve} of a vector field $X$ is a differential curve
	$\gamma\colon I \to M$ where $I$ is an open interval of $\R$ and $M$ a smooth
	manifold such that
	\[\gamma'(t)=X_\gamma(t)\quad \forall t \in I. \]
\end{definition}
Most of the time we are interested only by \emph{maximal integral curves}, which intuitively
are curves that can't be extended anymore. Let's see more in details what they are.
We say that an integral curve $\gamma$ starts at $p_0 \in M$ if $0 \in I$ and
$\gamma(0) = p_0$.
\begin{lemma}
	Let $\gamma_1\colon I_1 \to M$ and $\gamma_2\colon I_2 \to M$ starting at
	the same point $p_0$. Then the curves coincide on $I_1 \cap I_2$.
\end{lemma}
The proof is uses a well-know idea, so it's not reported. %TODO: magari non bluffare così perchè non hai voglia di dimostrarlo...
This lemma allows us to define the \emph{maximal integral curve starting at $p_0$}
as the union of all integral curves starting at $p_0$, since there are no
problem on intersections.


\subsubsection{Lie Bracket}
Last thing we need to define is an operation over the space of vector fields.
\begin{definition}[Lie bracket] \index{lie bracket}
	The \emph{Lie bracket} two smooth vector fields $X$ and $Y$ on $M$ is the map
	\begin{align*}
		[X,Y]\colon C^\infty(M) &\to C^\infty(M),\\
		f &\mapsto D_X\left(D_Y\left(f\right)\right) - D_Y\left(D_X\left(f\right)\right).
	\end{align*}
\end{definition}
Note that the Lie bracket is well-defined since it is a difference of composition
of $C^\infty$ functions, so it's $C^\infty$ too. For calling Lie bracket an operation
on the space of vector fields we need to prove that it is a vector field.
\begin{lemma}
	The Lie bracket of two vector fields is a vector field.
	\begin{proof}
		Let $X$ and $Y$ two vector fields. From the Lemma \ref{lem:vectorfieldequiv}
		it follows that we need to check only if $[X,Y]$ is linear and respects
		the Leibniz rule. Let $f,g \in  C^\infty(M)$ and $\alpha,\beta \in \R$.
		The linearity:
		\begin{align*}
			[X,Y](\alpha f + \beta g) 
			&= D_X\left(D_Y\left(\alpha f + \beta g\right)\right) -
			   D_Y\left(D_X\left(\alpha f + \beta g\right)\right) =\\
			&= D_X\left(\alpha D_Y\left(f\right) + \beta D_Y\left(g\right)\right) -
			   D_Y\left(\alpha D_X\left(f\right) + \beta D_Y\left(g\right)\right) =\\
			&= \alpha D_X(D_Y(f)) + \beta D_X(D_Y(g)) - \alpha D_Y(D_X(f)) - \beta D_Y(D_X(g)) =\\
			&= \alpha [X,Y](f) + \beta [X,Y](g).
		\end{align*}
		Leibniz rule:
		\begin{align*}
			[X,Y](fg)
			&= D_X(D_Y(fg)) - D_Y(D_X(fg)) =\\
			&= D_X(D_Y(f)g + fD_Y(g)) - D_Y(D_X(f)g + fD_X(g)) =\\
			&= D_X(D_Y(f)g) + D_X(fD_Y(g)) -  D_Y(D_X(f)g) - D_Y(fD_X(g)) =\\
			&= D_X(D_Y(f))g + D_Y(f)D_X(g) + D_X(f)D_Y(g) + fD_X(D_Y(g)) -\\
			&\quad - D_Y(D_X(f))g - D_X(f)D_Y(g) - D_Y(f)D_X(g) - fD_Y(D_X(g)) =\\
			&= f(D_X(D_Y(g)) - D_Y(D_X(g))) + (D_X(D_Y(f)) - D_Y(D_X(f)))g =\\
			&= f[X,Y] + [X,Y]g.
		\end{align*}
	\end{proof}
\end{lemma}

\subsection{Pushforward of vector fields}
Let $M_1$ and $M_2$ two  smooth manifolds and $\phi\colon M_1 \to M_2$ a smooth
map of them. One could think to use the differential of $\phi$ for defining a map
that ``transport'' vector fields from $M_1$ to  $M_2$. More formally, given
$q \in M_2$ and $g \in C^\infty(M_2)$ we define $d\phi\colon \X(M_1) \to \X(M_2)$
as
\begin{equation} \label{eq:defdiffvector}
	(d\phi(X))_q(g) = X_{f^{-1}(q)}(g \circ \phi)
\end{equation}
Without any other condition on $\phi$ \emph{this is a bad definition}. In fact,
if $\phi$ is not surjective it is not guarantee that $q \in \phi(M_1)$, so the
vector field is not defined at every point of $M_2$. Furthermore, if $\phi$ isn't
injective $f^{-1}(q)$ may not be unique, and we don't know how to choose one. We
need another definition to get what we want.
\begin{definition}[$\phi$-relation] \index{$\phi$-relation}
	Let $\phi\colon M_1 \to M_2$ is a smooth map of manifolds, $X \in \X(M_1)$ and
	$y \in \X(M_2)$ two vector fields. We say that $X$ and $Y$ are \emph{$phi$-related}
	if $\forall p \in M_1$
	\begin{equation} \label{eq:phirelation_a}
		d\phi_p(X_p) = Y_{F(p)},
	\end{equation}
	or equivalentely $\forall g \in C^\infty(M_2)$
	\begin{equation} \label{eq:phirelation_b}
		D_X(g \circ \phi) = D_Y(g) \circ \phi.
	\end{equation}
	If $M_1 = M_2$ and $X = Y$ we say that $X$ is \emph{$\phi$-invariant}.
\end{definition}
Most of the times we will work with diffeomorphism so the next lemmas are useful.
\begin{lemma}
	If $\phi\colon M_1 \to M_2$ is a diffeomorphism, then for each $X \in \X(M_1)$
	there is one and only one $Y \in \X(M_2)$ that is $\phi$-related to $X$.
	Moreover, the application defined in the equation \eqref{eq:defdiffvector} is
	a vector space isomorphism.
	\begin{proof}
		Since that diffeomorphism are bijective the equation \eqref{eq:phirelation_a}
		uniquely determine a vector field on $M_2$. It is smooth since
		from \eqref{eq:phirelation_b} we get $D_Y(g) = D_X(g \circ \phi) \circ \phi^{-1}$,
		which  is smooth for all smooth $g$ as a composition of smooth functions. \\
		%TODO: la parte sull'isomorfismo  di spazi vettoriali
	\end{proof}
\end{lemma}

The $\phi$-relation behaves well with the Lie bracket, as we will see in the next
lemma.
\begin{lemma} \label{lem:phirelationpreserved}
	The Lie bracket preserves the $\phi$-relation. In other words, given 
	$X, \bar{X} \in M_1$ which are $\phi$-related to $Y, \bar{Y} \in M_2$ respectively,
	then $[X,\bar{X}]$ is $\phi$-related with $[Y,\bar{Y}]$.
	\begin{proof}
		Let $g \in C^\infty(M_2)$. We have to prove the egualiance $*$:
		\begin{align*}
		  \left[X,\bar{X}\right](g \circ \phi) = 
		  D_X(D_{\bar{X}}(g \circ \phi)) - D_{\bar{X}}(D_X(g \circ \phi)) \stackrel{*}{=} \\
		  \stackrel{*}{=} \left[D_Y(D_{\bar{Y}}(g)) - D_{\bar{Y}}(D_Y(g))\right] \circ \phi =
		  \left(\left[Y,\bar{Y}\right](g)\right) \circ \phi.
		\end{align*}
		Note that
		\[ D_X(D_{\bar{X}}(g \circ \phi)) \stackrel{\bar{X}\text{ $\phi$-rel. }\bar{Y}}{=}
		   D_X(D_{\bar{Y}}(g) \circ \phi) \stackrel{X\text{ $\phi$-rel. }Y}{=}
		   D_Y(D_{\bar{Y}}(g))\circ \phi,\]
		and in the same way $D_{\bar{X}}(D_X(g \circ \phi)) = D_{\bar{Y}}(D_Y(g)) \circ \phi$.
		now it is sufficient to substitute in the previous equation to see that
		$*$ is verified.
	\end{proof}
\end{lemma}

\section{Lie groups}
\begin{definition}[Lie group] \index{Lie group}
	A \emph{Lie group} $\G$ is a smooth manifold which is also a group, such that
	its product and the function that map an element to its inverse are smooth.
	The dimension of $\G$ is its dimension as a manifold.
\end{definition}
In particulare note that, fixed $g_0 \in \G$, the function
\begin{align*}
	L_{g_0}\colon \G \to \G \\
	g \mapsto g_0 \cdot g
\end{align*}
is a diffeomorphism.
\subsection{Left invariant vector fields}
Since the Lie groups are both a manifold and a group, we can can define some objects
that involve both both these property.
\begin{definition}[Left Invariant Vector Field] \index{vector field!left invariant}
	A vector field $X$ is \emph{left-invariant} if it is $L_{g_0}$-invariant for
	all $g_0\in \G$.
\end{definition}
The importance of left invariaant vector fields will be explored more deeply in
the next section. For the moment let's see this theorem that prove that there
exists some left-invariant vector fields, but knowing its value at a single point
tell us the field everywhere.
\begin{theorem} \label{thm:livfuniquedeterminedbyidentity}
	Let $\G$ a Lie group, $e$ its identity element and $v \in T_e\G$. There
	exists an unique left invariant smooth vector field $X$ such that $X_e = v$.
	\begin{proof}
		The left-invariance (through equation \eqref{eq:phirelation_a})  tell us
		\begin{equation} \label{eq:linvexstension}
			X_g = \left(dL_g\right)_e\left(X_e\right),
		\end{equation}
		which clearly give us the value of the vector field at every point. This
		doesn't imply that the field is left invariant, so we have to check that
		\begin{align*}
			(dL_g)_h(X_h) &= (dL_g)_h((dL_h)_e(X_e)) = (dL_g)_{L_h(e)}((dL_h)_e(X_e)) = \\
			  &= \left((dL_g)_{L_h(e)} \circ (dL_h)_e\right)(X_e) \stackrel{\text{chain r.}}{=}
			    d(L_g \circ L_h)_e(X_e) = \\
			  &= d(L_{gh})_e(X_e) = X_{gh} = X_{L_g(h)},
		\end{align*}
		where we used the equation \eqref{eq:linvexstension} two times.
		%TODO: smoothness. 
		%Now we have to prove that the vector field is smooth. To do this we will
		%provea more general stantment: \emph{every left-invariant vector field
		%is smooth}.
	\end{proof}
\end{theorem}
Note that it is not necessary that the point where we know the vector field is
the identeity, but it could be everywhere on the manifold.
Lastly, note taht as particular case of Lemma \ref{lem:phirelationpreserved}
we have this result, which will be usefull later.
\begin{lemma} \label{lem:liebracketleftinvariant}
	The Lie bracket of two left invariant vector filed is left invariant.
\end{lemma}

\section{Lie algebras}
The concept of \emph{Lie algebra} is really general and it does not depend on
the definitions of Lie Group or manifold. As consequence we will see some general
definitions and only only after that we will procede to build the link between
Lie groups and Lie Algebras.
\begin{definition}[Lie algebra] \index{Lie group}
	A \emph{Lie algebra} $\g$ is a vector space over a field $\mathbb{F}$ which has a
	\emph{bracket} $[,]\colon \g \times \g \to \g$ such that bracket
	\begin{itemize}
		\item is \emph{linear}: $[av+bw,u] = a[v,u] + b [w,u] \quad \forall v,w,u \in \g$
		and $\forall a,b \in \mathbb{F}$;
		\item is \emph{antisymmetric}: $[v,w] = - [w,v] \quad \forall v,w \in \g$;
		\item respects the \emph{Jacobi identity}: 
			$[v,[w,u]] + [u,[v,w]] + [w,[u,v]] = 0 \quad \forall v,w,u \in \g$.
	\end{itemize}
\end{definition}
There are also the concepts of \emph{homeomorphism} and \emph{representations} for
the Lie algebras.
\begin{definition}[Morphism] \index{homomorphism!of Lie algebra}
	A \emph{homomorphism of Lie algebra} is a map $\psi\colon \g_1 \to \g_2$ such
	that:
	\begin{itemize} 
		\item it's \emph{linear};
		\item $\forall v, w \in \g_1$:  $\psi\left([v,w]\right)= \left[\psi(v), \psi(w)\right]$.
	\end{itemize}
	If $\psi$ it's also invertible is called \emph{isomorphism}.
\end{definition}

\begin{definition}[Representation] \index{representation!of Lie algebra}
	Let $V$ a vector space and $\g$ a Lie algebra. The set of endomorphism of $V$,
	denoted by $\End{V}$, is a Lie Alegra with the commutator\footnote{In case $V$ is
	finite the commutator is the usal matrix commutator.} $[X,Y] = XY - YX$.
	A \emph{Lie algebra representation} is a Lie algebra homomorphism
	$\rho\colon \g \to \End{V}$.\\
	A representation is called \emph{faithfull} if it is also injective.
\end{definition}

As last result of this section we present a theorem that allows to treat any abstract
finite Lie algebras as a set of matrices, which is easier to handle.
\begin{theorem}[Ado's theorem]
	Any finite-dimensional Lie algebra g has a faithful representation.
	In other words, any g is isomorphic to a Lie subalgebra\footnote{A subset of
	a Lie algebra closed under the bracket.} of $\Mat{N}{\R}$ for some integer $N$.
\end{theorem}
A nice proof can be found at \cite{terencetao}.

\section{Lie Algebra of a Lie Group}
As you may already know, given a Lie group, there's a Lie algebra, that describe
locally the group. In this section we will prove that the Lie algebra of a group
is the set of its left invariant vector fields, which we will call $\Lie{G}$.
\begin{lemma}
	$\Lie{G}$ is a vector space, with the same operations used for tangent space.
	\begin{proof}
		Since vector fields are functions from $C^\infty(\G)$ to itself, and this
		set of functions is a vector space, the only one thing we have to verify
		is that the set of left-invariant vector field is closed under vector
		space operations.
		Let $X, Y \in \X(\G)$ two left invariant vector fields and $g \in C^\infty(\G)$:
		\begin{align*}
		  D_{\alpha X + \beta Y}\left(g \circ L_{g_0}\right) &= 
		  \alpha D_X\left(g \circ L_{g_0}\right) + \beta D_Y\left(g \circ L_{g_0}\right) = \\
		  &=\alpha D_X\left(g\right) \circ L_{g_0} + \beta D_Y\left(g\right) \circ L_{g_0} = \\
		  &=\left(\alpha D_X\left(g\right) + \beta D_Y\left(g\right) \circ L_{g_0}\right) \circ L_{g_0} = \\
		  &=D_{\alpha X + \beta Y}\left(g\right) \circ L_{g_0},
		\end{align*}
		where the first and the last are true by definition of operation on
		derivations, and the second because $X$ and $Y$ are left-invariant.
	\end{proof}
\end{lemma}
\begin{lemma}
	Lie brackets of two vector fields as operation over the space of vector fields
	are linear, antisymmetric and respects Jacobi Identity.
	\begin{proof}
		Let $X, Y, Z \in \X(\G)$ and $g \in C^\infty(\G)$. \\
		Linearity:
		\begin{align*}
		  [\alpha X + \beta Y, Z](g) &= 
	      D_{\alpha X + \beta Y}(D_Z(g)) - D_Z(D_{\alpha X + \beta Y}(g)) = \\
		  &= \alpha D_X(D_Z(g)) + \beta D_Y(D_Z(g)) - D_Z(\alpha D_X(g) + \beta D_Y(g)) = \\
		  \text{linearity of $D_Z$} \quad &= \alpha D_X(D_Z(g)) + \beta D_Y(D_Z(g)) 
		   - \alpha D_Z(D_X(g)) - \beta D_Z(D_Y(g)) = \\
		  &= \alpha [X,Z](g) + \beta [Y,Z](g).
		\end{align*}
		Antisymmetry:
		\begin{align*}
		  [X,Y](g) &= D_X(D_Y(g)) - D_Y(D_X(g)) \\ &= - \left(- D_X(D_Y(g)) + D_Y(D_X(g)) \right) 
		        = - [Y,X](g)
		\end{align*}
		Jacobi Identity:
		\begin{align*}
			&[X,[Y,Z]](g) + [Z,[X,Y]](g) + [Y,[Z,X]](g) = \\
			  &= D_X(D_Y(D_Z(g))) - D_X(D_Z(D_Y(g))) - D_Y(D_Z(D_X(g)))) + D_Z(D_Y(D_X(g))) + \\
			  &+ D_Z(D_X(D_Y(g))) - D_Z(D_Y(D_X(g))) - D_X(D_Y(D_Z(g)))) + D_Y(D_X(D_Z(g))) + \\
			  &+ D_Y(D_Z(D_X(g))) - D_Y(D_X(D_Z(g))) - D_Z(D_X(D_Y(g)))) + D_X(D_Z(D_Y(g))) = \\
			  &= 0
		\end{align*}
	\end{proof}
\end{lemma}

From the two lemmas that we have just proved, along with Lemma
\ref{lem:liebracketleftinvariant}, it follows that $\Lie{\G}$ with the Lie bracket
is a Lie algebra. The elements of a basis $\left\{ e_i\right\}$ of $\Lie{\G}$ are
said to be the \emph{generators} of the algebra. 

It is not hard to belive knowing Theorem \ref{thm:livfuniquedeterminedbyidentity}
that the following theorem is true. The proof it's really easy, so it is not
reported.
\begin{theorem}
	The Lie algebra of Lie group $\G$ and the its tanget space at identity are isomorphic
	\[\Lie{\G} \cong T_e\G. \]
\end{theorem}
Note that this theorem tell us that the dimension of the Lie algebra is the same of
the Lie group. 

At the end we will present these three important theorems, without proof.
For a proof consult \cite{heidelberg}.
\begin{theorem}
	Let $\G$ and $\Ha$ two Lie groups, and $\varphi\colon \G \to \Ha$ a group homomorphism.
	Then $d\varphi_e\colon T_e\G \to T_e\Ha$ define a Lie algebra homomorphism.
\end{theorem}
Note that this theorem imply that if $\G \cong \Ha$ then $\Lie{\G} \cong \Lie{\Ha}$.
The opposite is clearly false: two groups can be locally isomorphic, but not globally.
\begin{theorem}
	Let $\psi\colon\G\to\Ha$ a Lie algebra homomorphism, with $\G$ simply connected.
	Then there exist a unique group homomorphism $\varphi$ such that $\psi = d\varphi$.
\end{theorem}
\begin{theorem}[Lie's third theorem]
	Every finite-dimensional real Lie algebra is the Lie algebra of some simply
	connected Lie group.
\end{theorem}	

\subsection{Exponential map}
The exponential map is probably the most important concept of the Lie theory since
it give us a way to reconstruct the Lie group (or at lest its connected component of
the identity) given the Lie algebra. We need to define a few new concepts before
saying what the exponential map is.
\begin{definition}[one-parameter subgroup] \index{one-parameter!subgroup}
	Let $\G$ a Lie group. A \emph{one-parameter subgroup} is a group homomorphism
	$\left(\R, +\right) \to \G$.
\end{definition}
The important result that we need to define the exponential map is the following theorem.
\begin{theorem}
	 The one-parameter subgroups of a Lie group are exactly the maximal integral
	 curves of its left-invariant vector fields, starting at $e$.
\end{theorem}
The proof can be found in \cite{chicago}.
Since every vector field has a unique maximal integral curve and every $v \in T_e\G$
corresponds to a unique left-invariant vector field, the following is well-defined.
\begin{definition}[Exponential map] \index{exponential map}
	Let $\gamma_v$ the \emph{unique} maximal integral curve such that $\gamma_v'(0)=v\in T_e\G$
	The \emph{exponential map} is a function
	\begin{align*}
		\exp\colon \Lie{\G} &\to \G \\
		v &\mapsto \gamma_v(1)
	\end{align*}
\end{definition}
This definition may seem abstact but most of the times (when the Lie group is a
matrix group) the exponential map is simply the matrix exponentiation. Lastly we
present this result, that show how the exponential map connctes a Lie group and
its Lie algebra.
\begin{theorem}
	Let $\G$ be a Lie group and $\g$ its Lie algebra. There exists a neighborhood
	$U \subset \g$ contaning 0 and a neighborhood $V \subset \G$ containing $e$ 
	such that 
	\[\exp|_U\colon U \to V\]
	is a diffeomorphism.
\end{theorem}
Again the proof can be found in \cite{chicago}.
\subsubsection{Properties}
Here is a list of some properties of the exponential map:
\begin{itemize}
	\item $\exp\left((t+s)X\right) = \exp(tX)\exp(sX)$;
	\item $\exp(-X) = \left[\exp(X)\right]^{-1}$;
	\item if $[X,Y]=0$ then $\exp(X+Y)=\exp(X)\exp(Y)$;
	\item the image of the exponential maps lies on the connected component of
	      the identity;
	\item Every element $g$ of the connected component of the identity can be 
		  expressed as
		  \[g = \exp(X_1) \exp(X_2) \cdots \exp(X_n), \quad X_j \in \g; \]
  \item even if the group is connected the map is not necessarly surjective. 
     But if the Lie group is \emph{compact}, then the exponential map is connected on
      the connected   component of  the identity. The proof can be easily found online.
      
  \item Here is a list of common Lie Groups where the exponential map is surjective
        (on the   identity connectd component):
      \begin{itemize}
        \item $O(n)$, $SO(n)$, $U(n)$ and $SU(n)$ (they are \emph{compact}).
        \item Lorentz Group ($SO^+(3;1)$)
      \end{itemize}
\end{itemize}


\section{Other topics}
%questi sono altri argomenti fatti male sulle dispenese (e spero che siano anche gli
%ultimi). Per il momento non ho voglia di texxarli, spero mi verrà in futuro, tanto è
%roba da un pomeriggio
\subsection{Adjoint representation}
\subsection{Killing Form}
\subsection{Casemir Operator}
% COMMENTI:
% - una grossa mancanza sono i numeri complessi: tutti gli spazi vettoriali che ci
%   sono in questa dispensa sono su R, mentre poi nella realtà si lavora con C, come
%   si vede ad esmpio nella definizione di generatore di Ken. Sarebbe bello spiegare
%   un po' sta cosa.
% - Ken abusa spesso dell'esponenziale (tipo esponenzia combinazioni lineari di generatori).
%   bisognerebbe capire cosa sta facendo davvero e spiegarlo in modo un po'piú rigoroso.
%   Secondo me lui di fatto lavora solo con rappresentazioni e quindi fa il cazzo che gli
%   pare tanto sono tutte matrici, però non sono sicuro. Spero di capirlo più avanti.